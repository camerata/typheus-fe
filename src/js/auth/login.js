import React from "react";
import {TextField} from "../common/inputs";


class LoginForm extends React.Component {
    render() {
        return <form className={"login-form panel"}>
            <TextField placeholder="Username"/>
            <TextField placeholder="Password"/>
        </form>;
    }
}

export class Login extends React.Component {
    render() {
        return <div className={"container"}><LoginForm /></div>
    }
}