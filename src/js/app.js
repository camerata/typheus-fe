import React from 'react';


import {Login, LoginForm} from "./auth/login";
import {Header} from "./common/header";
import {Navbar} from "./common/navbar";
import {Content} from "./common/content";
import {Route, Switch} from "react-router-dom";
import {Main} from "./main/main";
import {Tasks} from "./tasks/tasks";

export class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            auth: {
                jwt: null,
                currentUser: {},
                isLoggedIn: () => this.state.jwt,
            }

        }
    }

    handleLogin(jwt, user) {
        this.setState({jwt: jwt, currentUser: user})
    }

    render() {
        return <div>
            <Header auth={ this.state.auth }/>
            <Switch>
                <Route exact path='/' component={() => (<Main auth={this.state.auth} />)}/>
                <Route path='/login' component={() => (<Login auth={this.state.auth} />)}/>
                <Route path='/tasks' component={() => (<Tasks auth={this.state.auth}  />)}/>
            </Switch>


        </div>
    }
}