import React from "react";
import axios from "axios";
import {Loading} from "../common/loading";
import {Navbar} from "../common/navbar";
import {Content} from "../common/content";
import {Route, Switch} from "react-router-dom";


class TaskActionBar extends React.Component {
    render() {
        return <div className={"task-list__item__actions"}>
            <div>
                Try
            </div>
            <div>
                Hide
            </div>
        </div>
    }
}


class TaskItem extends React.Component {
    render() {
        return <div className={"task-list__item panel"}>
            <div className={"task-list__item__title title"}>
                { this.props.task.title }
            </div>
            <div className={"task-list__item__description"}>
                { this.props.task.description }
            </div>
            <TaskActionBar/>
        </div>
    }
}

class TaskList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: null
        }
    }

    componentDidMount(){
        axios.get(this.props.url,
            {
                headers: {'Authorization': 'Token'}
            }
        ).then(
            (response) => this.setState({tasks: response.data.tasks})
        )
    }

    render() {
        const tasks = this.state.tasks;
        return <div className={"task-list"}>
            { tasks ? tasks.map((task) => <TaskItem task={{title: task.title, description: task.description}}/>) : <Loading />}
        </div>
    }
}

export class Tasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: null
        }
    }

    componentDidMount(){
        axios.get("http://localhost:5000/categories",
            {
                headers: {'Authorization': 'Token'}
            }
        ).then(
            (response) => this.setState({options: response.data.categories.map((category) => {category.href = `/tasks/category/${category.title}`; return category;}) })
        )
    }

    render() {
        return <Content>
            <Navbar options={ this.state.options } />
            <Switch>
                <Route exact path='/tasks' component={() => <TaskList url={"http://localhost:5000/tasks"}/>}/>
                <Route path='/tasks/category/:name' component={(props) => <TaskList url={`http://localhost:5000/tasks?category=${props.match.params.name}`}/>}/>
            </Switch>

        </Content>
    }
}