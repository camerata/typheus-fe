import React from "react";
import {Thumbnail} from "../images/thumbnail";
import {Loading} from "./loading";
import {Link} from "react-router-dom";

class NavbarOption extends React.Component {
    render() {
        return <div className="navbar__item">
            <Thumbnail src=""/>
            <Link to={ this.props.href } className="navbar__item__title title">
                { this.props.name }
            </Link>

        </div>
    }
}

export class Navbar extends React.Component {
    render() {
        const options = this.props.options;
        return <div className="navbar panel">
            { options ? options.map((option) => <NavbarOption name={ option.title } href={option.href} />) : <Loading/>}
        </div>
    }
}
