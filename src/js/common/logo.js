import React from "react";
import {withRouter} from "react-router-dom";


class Logo extends React.Component {

    onClick() {
        this.props.history.push("/");
    }

    render() {
        return <div className="logo" onClick={ this.onClick.bind(this) }>
            <img className="default" src="public/img/logo.svg"/>
            <img className="pressed" src="public/img/logo_pressed.svg"/>
            <span>Typheus</span>
        </div>
    }

}

export default withRouter(Logo);

