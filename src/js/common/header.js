import React from "react";
import { Link } from 'react-router-dom'

import Logo from "./logo";

class HeaderLink extends React.Component {
    render() {
        return <div className="header__link">
            <Link to={this.props.href}>{ this.props.name }</Link>
        </div>
    }
}

export class Header extends React.Component {
    render() {
        let authLink = null;
        if (this.props.auth.isLoggedIn())
            authLink = <HeaderLink href="/profile" name={ this.props.auth.currentUser.username } />;
        else
            authLink = <HeaderLink href="/login" name={"Login"} />;
        return <header className="header">
            <Logo />
            { this.props.auth.isLoggedIn() }
            { authLink }
        </header>
    }

}
