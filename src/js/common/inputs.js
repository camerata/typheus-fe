import React from "react";


export class TextField extends React.Component {
    render() {
        return <div className={"input"}>
                    <label htmlFor={ this.props.name } >{ this.props.label }</label>
                    <input id={ this.props.name } placeholder={ this.props.placeholder } type="text"/>
                </div>
    }
}
