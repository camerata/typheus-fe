import React from "react";


export class Thumbnail extends React.Component {
    render() {
        return <img className="image__thumbnail" src={ this.props.src } />
    }
}
