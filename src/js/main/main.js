import React from "react";
import {Content} from "../common/content";
import {Navbar} from "../common/navbar";
import {Tasks} from "../tasks/tasks";


export class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            choices: [
                {
                    title: "Main page",
                    href: "/"
                },
                {
                    title: "Tasks",
                    href: "/tasks"
                },
                {
                    title: "Profile",
                    href: "/profile"
                },
            ]
        }
    }
    render() {
        return <Content>
                  <Navbar options={ this.state.choices } />
               </Content>
    }
}