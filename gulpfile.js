const gulp = require('gulp'),
      stylus = require('gulp-stylus'),
      webpack = require('webpack'),
      webpackStream = require('webpack-stream'),
      livereload = require('gulp-livereload'),
      server = require('webpack-dev-server');

const config = require('./webpack.config.js');


gulp.task('stylus', function() {
    return gulp.src('./src/stylus/main.styl')
        .pipe(stylus())
        .pipe(gulp.dest('dist/css'));
});


gulp.task('webpack', function() {
    return gulp.src('./src/js/typheus.js')
        .pipe(webpackStream(config, webpack))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('src/stylus/**/*.styl', ['stylus']);
    gulp.watch('src/js/**/*.js', ['webpack']);
});

gulp.task('server', function() {

    new server(webpack(config), {}).listen(8080, "localhost", function(err) {
        //
    });
});